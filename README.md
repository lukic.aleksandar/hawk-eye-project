# hawk-eye-project
Kako bi se pokrenula aplikacija neophodno je instalirati sledeće: <br/>
1) Python v2.7, https://www.python.org/downloads/ <br/>
2) OpenCv v3.0, http://opencv.org/downloads.html . Pored toga neophodno je kopiranje opencv_ffmpeg(verzija openCV-ja).dll u folder u kom je instaliran Python 2.7 (root). Ovaj dll se nalazi u (mesto gde je instaliran opencv)/build/x86/v12/ ili  (mesto gde je instaliran opencv)/build/x64/v12/ <br/>
3) Scipy v0.16, https://sourceforge.net/projects/scipy/files/scipy/0.16.0/ <br/>
4) Numpy v1.10, https://sourceforge.net/projects/numpy/files/NumPy/1.10.1/  <br/>
5) matplotlib v1.5 + sve što matplotlib zahteva, http://matplotlib.org/users/installing.html  
