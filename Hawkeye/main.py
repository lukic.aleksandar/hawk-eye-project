from balltrack.balltracking import Algorithms
from balltrack.balltracking import TennisCourt
from videoconf.video_analysis import MorphologyEnumerations
from videoconf.video_analysis import VideoAnalyzer
from videoconf.camera_calibration import Camera
from matplotlib.path import Path
import matplotlib.pyplot as plt
import cv2

#Initializing tennis court shapes
tc1 = TennisCourt([[70,540],[1235, 518],[838,389],[452,394], [70,540]],[Path.MOVETO, Path.LINETO,Path.LINETO, Path.LINETO, Path.CLOSEPOLY])
tc2 = TennisCourt([[40,582],[1280, 544],[776,410],[335,408], [40, 582]], [Path.MOVETO, Path.LINETO,Path.LINETO, Path.LINETO, Path.CLOSEPOLY])

#Setting parameters for first camera
cam1 = Camera("Videos/2.6.mov")
cam1.readCalibrationParams('videoconf/D610.json')
video1 = VideoAnalyzer(cam1)
video1.sizeBoundaries = ((6,50),(6,50))
video1.lowerColorBnd = (30,20,50)
video1.upperColorBnd = (60,255,255)
video1.ellipseFuzzySet.x = [0,1,2]
video1.ellipseFuzzySet.y = [0,1,0]
video1.ellipseFuzzySet.threshold = 0.65
video1.colorFuzzySet.x = [0,1,1.05]
video1.colorFuzzySet.y = [0,1,0]
video1.colorFuzzySet.threshold = 0.5
video1.contourThreshold = 0.5
video1.imageAnalyzer.setKernel(MorphologyEnumerations.ELLIPSE,(3,3))
video1.morphologyArray = [MorphologyEnumerations.OPEN]
result1, images1 = video1.analyze()

#Setting parameters for second camera
cam2 = Camera("Videos/1.6.mov")
cam2.readCalibrationParams('videoconf/D7100.json')
video2 = VideoAnalyzer(cam2)  #23, 24,33, 6, 9, 10
video2.sizeBoundaries = ((7, 50), (7, 50))
video2.lowerColorBnd = (30, 30, 100)
video2.upperColorBnd = (60, 255, 255)
video2.ellipseFuzzySet.x = [0, 1, 2]
video2.ellipseFuzzySet.y = [0, 1, 0]
video2.ellipseFuzzySet.threshold = 0.65
video2.colorFuzzySet.x = [0, 1, 1.05]
video2.colorFuzzySet.y = [0, 1, 0]
video2.colorFuzzySet.threshold = 0.5
video2.contourThreshold = 0.5
video2.imageAnalyzer.setKernel(MorphologyEnumerations.ELLIPSE, (3, 3))
video2.morphologyArray = [MorphologyEnumerations.OPEN]
result2, images2 = video2.analyze()

cv2.destroyAllWindows()

alg = Algorithms({'video1': tc1, 'video2': tc2})
res = alg.prepareData(result1, result2)

if res != False:

    positions = alg.getBallFallPosition()
    alg.showVideoAfterInterp(images1, 'video1')
    alg.showVideoAfterInterp(images2, 'video2')

    if positions is not None:
        fallen =  alg.isBallIn(positions[0], positions[1])
        alg.tennisCourts['video1'].plotCourt(positions[0], fallen)
        alg.tennisCourts['video2'].plotCourt(positions[1], fallen)
        plt.show()

'''
  p1 = [p[0] for p in alg.videoAnalysisData]
    p2 = [p[1] for p in alg.videoAnalysisData]

    p1 = np.array([p[0] for p in p1], np.float32)
    p2 = np.array([p[0] for p in p2], np.float32)

    F, mask = cv2.findFundamentalMat(p1,p2)

    pp1 = np.array([502.82095337,  331.46795654, 1], np.float32)
    pp2 = np.array([441.63522339,  331.16293335, 1],np.float32)
    pp2 = np.transpose(pp2)

    U, D, V = np.linalg.svd(F, full_matrices=True)
    V = np.transpose(V)
    W = np.matrix([[0, -1, 0], [1, 0, 0], [0, 0, 1]],np.float32)
    #W = np.transpose(W)
    temp =np.dot(U,W)
    temp = np.dot(temp,V)
    P2 = np.c_[temp, U[:,2]]

    P2 = np.matrix([[ 0.89265209 , 0.21510324, -0.39610964,  0.89454561],
     [ 0.45052095, -0.3979882  , 0.79914721,  0.4469717 ],
     [ 0.0142522,  -0.89181612,-0.45217351,  0.00211024]], np.float32)


    P1 = np.matrix([[1.0, 0.0, 0.0, 0.0] ,[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0]], np.float32)
    #P2 = np.matrix([[0.9271, 0.1721, -0.3329, 0.9273],[ 0.3747, -0.4402, 0.8160, 0.3743],[ 0.0061, 0.8813, 0.4726, 0.0021]], np.float32)
    #P2 = np.matrix([[0.9275, -0.1783, 0.3286, 0.9273],[ 0.3739, 0.4377, -0.8177, 0.3743],[ -0.0020, -0.8813, -0.4726, 0.0021]], np.float32)
    #alg.triangulatePoints(P1,P2)
'''