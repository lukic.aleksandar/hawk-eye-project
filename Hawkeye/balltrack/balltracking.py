import cv2
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import matplotlib.patches as patches
import matplotlib.path as mplPath
import matplotlib

class Algorithms:

    def __init__(self, tennisCourts = None):
        self.triangulationData = []
        self.videoAnalysisData = []
        self.splines = {}
        self.frameBndTranslated = {}
        self.frameBnds = {}
        self.tennisCourts = tennisCourts

    def prepareData(self, video1AnalysisResult, video2AnalysisResult):
        x = 0

        if(video1AnalysisResult is None or len(video1AnalysisResult) == 0) or\
            (video2AnalysisResult is None or len(video2AnalysisResult) == 0):
            return False

        (splX1, splY1),(splX2, splY2) = self.interpolateXYPoly(video1AnalysisResult, video2AnalysisResult, 3)

        if splX1 is  None or splY1 is None:
            return False

        startFrame1, startFrame2, endFrame1, endFrame2 = (None, None, None, None)
        foundFirst = False

        for i in range(0, len(video1AnalysisResult)):
            for j in range(x, len(video2AnalysisResult)):
                if(video1AnalysisResult[i][4] == video2AnalysisResult[j][4]):

                    if foundFirst is False:
                        startFrame1 = video1AnalysisResult[i][4]
                        startFrame2 = video2AnalysisResult[j][4]
                        foundFirst = True

                    self.videoAnalysisData.append((video1AnalysisResult[i], video2AnalysisResult[j]))
                    endFrame1 = video1AnalysisResult[i][4]
                    endFrame2 = video2AnalysisResult[j][4]
                    x = j + 1
                    break

        self.frameBnds['video1'] = (startFrame1, endFrame1)
        self.frameBnds['video2'] = (startFrame2, endFrame2)

        startFrame1 = startFrame1 - video1AnalysisResult[0][4]
        endFrame1 = endFrame1 - video1AnalysisResult[0][4]
        startFrame2 = startFrame2 - video2AnalysisResult[0][4]
        endFrame2 = endFrame2 - video2AnalysisResult[0][4]

        self.splines['video1'] = (splX1, splY1)
        self.splines['video2'] = (splX2, splY2)
        self.frameBndTranslated['video1'] = (startFrame1, endFrame1)
        self.frameBndTranslated['video2'] = (startFrame2, endFrame2)

        video1Linspace = np.linspace(startFrame1, endFrame1, 1000)
        video2Linspace = np.linspace(startFrame2, endFrame2, 1000)

        points1 = np.matrix([splX1(video1Linspace), -splY1(video1Linspace)], np.float32)
        points2 = np.matrix([splX2(video2Linspace), -splY2(video2Linspace)], np.float32)

        self.triangulationData.append((points1, points2))

        return True

    def getBallFallPosition(self):

        if len(self.frameBndTranslated) == 0:
            return None

        video1Linspace = np.linspace(self.frameBndTranslated['video1'][0], self.frameBndTranslated['video1'][1], 1000000)
        video2Linspace = np.linspace(self.frameBndTranslated['video2'][0], self.frameBndTranslated['video2'][1], 1000000)

        dy1 = self.splines['video1'][1].derivative()
        dy1Eval = dy1(video1Linspace)
        roots1 = self.__getRoots__(dy1Eval, video1Linspace)

        dy2 = self.splines['video2'][1].derivative()
        dy2Eval = dy2(video2Linspace)
        roots2 = self.__getRoots__(dy2Eval, video2Linspace)

        if (len(roots1) == 0) or (len(roots2) == 0):
            return None

        p1 = []
        for p in roots1:
            if(dy1(p-0.5) < 0):
                p1.append(p)
        p2 = []
        for p in roots2:
            if(dy2(p-0.5) < 0):
                p2.append(p)

        root1 = p1[0]
        for p in p1:
            if p < root1:
                root1 = p

        root2 = p2[0]
        for p in p2:
            if p < root2:
                root2 = p

        return (+self.splines['video1'][0](root1), -self.splines['video1'][1](root1)),\
               (+self.splines['video2'][0](root2), -self.splines['video2'][1](root2))

    def __getRoots__(self, dy, linspace, tol = 1e-3):
        root_index = np.where((dy>-tol)&(dy<tol))
        root = linspace[root_index]
        root = set(np.round(root, decimals=2).tolist())
        root = np.array(list(root))
        return root

    def interpolateXYPoly(self, video1AnalysisResult, video2AnalysisResult = None, n = 2):

        if video1AnalysisResult is None or(len(video1AnalysisResult) == 0):
            return (None, None),(None, None)

        points = [p[0] for p in video1AnalysisResult]

        xvals = np.array([p[0] for p in points], np.float32)
        yvals = np.array([-p[1] for p in points], np.float32)

        interpAxis = np.array([p[4] for p in video1AnalysisResult], np.int32)
        interpAxis = interpAxis - interpAxis[0]

        splX = interpolate.UnivariateSpline(interpAxis, xvals, k = n)
        splY = interpolate.UnivariateSpline(interpAxis, yvals, k = n)

        resSplx = None
        resSplY = None

        if  video2AnalysisResult is not None:
            (resSplx, resSplY), (a, b) = self.interpolateXYPoly(video2AnalysisResult, None, n)

        return (splX, splY) , (resSplx, resSplY)


    def isBallIn(self, video1Pos, video2Pos):
        v1 = self.tennisCourts['video1'].isBallIn(video1Pos)
        v2 = self.tennisCourts['video2'].isBallIn(video2Pos)

        if v1 == v2:
            return v1
        else:
            return  None

    def triangulatePoints(self, firstProjMtx, secondProjMtx):

        result = cv2.triangulatePoints(firstProjMtx, secondProjMtx, self.triangulationData[0][0], self.triangulationData[0][1])

        result = result/result[3]

        fig = plt.figure()
        ax = fig.add_subplot(111, projection = '3d')
        ax.plot_wireframe(result[0],result[1],result[2])

        plt.show()
        for x in result:
            #print x
            print '--------------------------'

    def showVideoAfterInterp(self, images, video):

        if video != 'video1' and video != 'video2':
            return None

        startFrame = self.frameBnds[video][0]
        endFrame = self.frameBnds[video][1]
        subFrame = self.frameBndTranslated[video][0]

        result = []
        for i, img in enumerate(images):
            image = img.copy()

            for j in result:
                cv2.circle(image, j, 3, (0, 255, 255), cv2.FILLED)

            if i >= startFrame and i <= endFrame:
                cv2.circle(image, (int(self.splines[video][0](i - startFrame + subFrame)),int( -self.splines[video][1](i - startFrame + subFrame))), 3, (0,255, 255), cv2.FILLED)
                result.append((int(self.splines[video][0](i - startFrame + subFrame)),int( -self.splines[video][1](i - startFrame + subFrame))))

            cv2.imshow(video, image)

            if cv2.waitKey(60) & 0xFF == ord('q'):
                break
        cv2.destroyAllWindows()

# cv2.line(origImg, (50,582),(1270, 544),(0,0,0),2) leva slika
'''   cv2.line(origImg, (40,582),(1280, 544),(0,0,0),1)
                cv2.line(origImg, (1280,544), (776,410),(0,0,0),1)
                cv2.line(origImg, (776,410),(335,408),(0,0,0),1)
                cv2.line(origImg, (335,408),(40,582) ,(0,0,0),1)
'''

'''
 cv2.line(origImg, (70,540),(1235, 518),(0,0,0),1)
                cv2.line(origImg, (1235, 518), (838,389),(0,0,0),1)
                cv2.line(origImg, (838,389),(452,394),(0,0,0),1)
                cv2.line(origImg, (452,394),(70,540) ,(0,0,0),1)
'''
class TennisCourt:

    def __init__(self, verticies, codes):
        self.poly = mplPath.Path(verticies, codes)
    def isBallIn(self, position):
        return  self.poly.contains_point(position)

    def plotCourt(self, ball = None, fallen = None):

        fig = plt.figure()
        ax = fig.add_subplot(111)
        patch = patches.PathPatch(self.poly, facecolor = (0.34, 0.44, 0.49), lw = 2)
        ax.add_patch(patch)
        plt.ylim(600,200)
        xs, ys = zip(*self.poly.vertices)
        ax.plot(xs, ys, 'x', lw=2, color='black', ms=10)
        if ball is not None:
            ax.text(ball[0], ball[1], 'o')

        if fallen is None:
            ax.text(600,250,'ERROR', fontsize = 40)
        elif fallen == True:
            ax.text(600, 250,'IN', fontsize = 40)
        elif fallen == False:
            ax.text(600, 250,'OUT', fontsize = 40)
