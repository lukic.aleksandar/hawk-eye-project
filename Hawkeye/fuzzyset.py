class FuzzySet:

    def __init__(self,xvals,yvals):
       self.x = xvals
       self.y = yvals
       self.threshold = 0

    def __init__(self):
        self.x = []
        self.y = []
        self.threshold = 0

    def addTuple(self,x,y):
        self.x.append(x)
        self.y.append(y)

    def fitLinear(self,xval):

        if(len(self.x) == 0 or len(self.y) == 0):
            return None;

        if(xval < self.x[0]):
            return self.y[0]
        elif(xval > self.x[len(self.x)-1]):
            return self.y[len(self.y)-1]
        else:
            for i in range(0,len(self.x)-1):
                if(self.x[i]<=xval <=self.x[i+1]):
                    return (self.y[i+1]-self.y[i])/(self.x[i+1]-self.x[i])*(xval-self.x[i])+self.y[i]
