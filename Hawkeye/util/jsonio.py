import os
import json

class JSONParser:

    def parseCameraConf(self, confFile, id):

        if not os.path.isfile(confFile):
            print 'no file:', confFile
            return None;

        conf = open(confFile,'r')
        jsonObj = json.load(conf)
        result = None
        conf.close()
        for cam in jsonObj['cameras']:
            if cam['id'] == id:
                result = cam

        return result

    def parse(self,confFile):

        if not os.path.isfile(confFile):
            print 'no file', confFile
            return None

        conf = open(confFile,'r')
        jsonObj = json.load(conf)
        conf.close()

        return jsonObj


    def writeJSON(self,jsonObj,output):

        conf = open(output,'w')
        conf.write(jsonObj)
        conf.close()
