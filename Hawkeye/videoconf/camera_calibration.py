import numpy as np
import cv2
import json
from util.jsonio import JSONParser


class Camera:

    def __init__(self, capture, pos = None, calmatrix = None, distort = None):
        self.capture = cv2.VideoCapture(capture)
        self.id = capture
        self.position = pos
        self.frameRate = self.capture.get(cv2.CAP_PROP_FPS)
        self.resolution = (self.capture.get(cv2.CAP_PROP_FRAME_WIDTH),
                           self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.calibrationMatrix = calmatrix
        self.distortCoefs = distort
        self.inverseCalibrationMatrix = None

    def calibrate(self, confFile, output):
        parser = JSONParser()
        jsonObj = parser.parseCameraConf(confFile, self.id)

        print 'Calibration status: parsing configuration file'

        if jsonObj is None:
            print 'Wrong conf file'
            return  None

        print 'Calibration status: parsing finished'

        patternSize = (jsonObj['patternWidth']-1,jsonObj['patternHeight']-1)
        patternPoints = np.zeros((np.prod(patternSize), 3), np.float32)
        patternPoints[:, :2] = np.indices(patternSize).T.reshape(-1, 2)
        patternPoints *= jsonObj['squareSize']

        w, h = self.resolution[0], self.resolution[1]
        objPoints = []
        imgPoints = []
        frameCnt = 0
        frameDelay = int((jsonObj['inputDelay']/1000.0)*60)

        print 'Calibration status: selecting frames for calibration'

        while(1):
            ret, frame = self.capture.read()
            if (ret is True) and ((frameCnt % frameDelay) == 0):

                if(jsonObj['flipAroundHorizontalAxis'] is True):
                    cv2.flip(frame, 0, frame)

                found, corners = cv2.findChessboardCorners(frame,patternSize)

                if found:
                    term = (cv2.TERM_CRITERIA_EPS*cv2.TERM_CRITERIA_COUNT, 30, 0.1)
                    cvtFrame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
                    cv2.cornerSubPix(cvtFrame,corners,(5,5),(-1,-1), term)
                else:
                    continue

                imgPoints.append(corners.reshape(-1,2))
                objPoints.append(patternPoints)

                if(len(imgPoints) == jsonObj['numOfFrames']):
                    break

            frameCnt += 1

        print 'Calibration status: frames selected'
        print 'Calibration status: started calibration'

        rms, cameraMatrix, distCoefs, rvecs, tvecs = cv2.calibrateCamera(objPoints, imgPoints, (int(w), int(h)), self.getFlags(jsonObj), None)

        print 'Calibration status: calibration finished'

        self.calibrationMatrix = cameraMatrix
        ret, self.inverseCalibrationMatrix = cv2.invert(cameraMatrix)
        self.distortCoefs = distCoefs

        print "Calibration status: writing results to file"
        object = {'id':self.id, 'cameraMatrix': cameraMatrix.tolist(), 'distortedCoefficients': distCoefs.tolist()}
        jsonObj = json.dumps(object)
        parser.writeJSON(jsonObj,output)

        print("RMS:", rms)

    def __del__(self):
        self.capture.release()

    def readCalibrationParams(self, path):
        parser = JSONParser()
        jsonObj = parser.parse(path)

        self.calibrationMatrix = np.array(jsonObj['cameraMatrix'],np.float32)
        ret, self.inverseCalibrationMatrix = cv2.invert(self.calibrationMatrix)
        self.distortCoefs = np.array(jsonObj['distortedCoefficients'], np.float32)

    def undistortedFrames(self):
        cnt = 0

        while(1):
            ret, frame = self.capture.read()
            w,  h = int(self.resolution[0]), int(self.resolution[1])
            newcameramtx, roi = cv2.getOptimalNewCameraMatrix(self.calibrationMatrix,  self.distortCoefs, (w, h), 1, (w, h))
            x,y,w,h = roi
            if ret:
                dst = cv2.undistort(frame, self.calibrationMatrix, self.distortCoefs,None,newcameramtx)
                if(roi != (0,0,0,0)):
                    dst = dst[y:y+h,x:x+w]

                cv2.imshow('frame',dst)
            if  cv2.waitKey(3) & 0xFF == ord('q') or cnt == self.capture.get(cv2.CAP_PROP_FRAME_COUNT):
                break

            cnt += 1

    def getFlags(self, jsonObj):

        flags = None

        if(jsonObj['fixAspectRatio'] is True):
            flags = cv2.CALIB_FIX_ASPECT_RATIO

        if(jsonObj['tangentDistortIsZero'] is True):
            if flags is not None:
                flags = flags | cv2.CALIB_ZERO_TANGENT_DIST
            else:
                flags = cv2.CALIB_ZERO_TANGENT_DIST

        if(jsonObj['fixPrincipalPointAtCenter'] is True):
            if flags is not None:
                flags = flags | cv2.CALIB_FIX_PRINCIPAL_POINT
            else:
                flags = cv2.CALIB_FIX_PRINCIPAL_POINT

        return flags

#cam = Camera('../Videos/D610.mov')
#cam.calibrate('camera_conf.json','pera.json')
#cam.readCalibrationParams('D610.json')
#cam.undistortedFrames()