import numpy as np
import cv2
from fuzzyset import FuzzySet
from videoconf.camera_calibration import Camera

class VideoAnalyzer:

    def __init__(self, camera):
        self.Camera = camera
        self.ellipseFuzzySet = FuzzySet()#([0,1,2.5],[0,1,0])
        self.colorFuzzySet = FuzzySet()#([0,1,1.05],[0,1,0])
        self.lowerColorBnd = (0, 0, 0)
        self.upperColorBnd = (180, 255, 255)
        self.bgs = cv2.createBackgroundSubtractorMOG2()
        self.bgsLearningRate = 0.1
        self.imageAnalyzer = ImageAnalyzer()
        self.morphologyArray = []
        self.sizeBoundaries = ((0, 0), (0, 0))
        self.contourThreshold = 0.5

    def analyze(self):
        cnt = 0
        fcnt = 0
        result = []
        images = []

        while(1):

            ret, frame = self.Camera.capture.read()

            if(ret != False):

                if self.Camera.calibrationMatrix is not None:
                    w,  h = int(self.Camera.resolution[0]), int(self.Camera.resolution[1])
                    newCamMatrix, roi = cv2.getOptimalNewCameraMatrix(self.Camera.calibrationMatrix, self.Camera.distortCoefs, (w, h), 1, (w, h))
                    frame = cv2.undistort(frame, self.Camera.calibrationMatrix, self.Camera.distortCoefs, None, newCamMatrix)

                images.append(frame.copy())

                rectImg = frame.copy()
                hsvImg = frame.copy()

                hsvImg = cv2.cvtColor(hsvImg, cv2.COLOR_BGR2HSV)
                hsvMask = cv2.inRange(hsvImg, self.lowerColorBnd, self.upperColorBnd)  #(30,10,110), (60,255,255))

                frame = self.bgs.apply(frame, learningRate = self.bgsLearningRate)

                for morph in self.morphologyArray:
                    frame = self.imageAnalyzer.morphology(frame, morph)

                image, contours, hierarchy = cv2.findContours(frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

                fittingContours = []
                for i,contour in enumerate(contours):
                    x,y,w,h = cv2.boundingRect(contour)
                    if(w >= self.sizeBoundaries[0][0] and w <= self.sizeBoundaries[0][1]
                       and h >= self.sizeBoundaries[1][0] and h <= self.sizeBoundaries[1][1]):

                        rotatedRect = cv2.fitEllipse(contour)
                        center = (rotatedRect[0][0], rotatedRect[0][1])
                        size = (rotatedRect[1][0], rotatedRect[1][1])
                        angle = (rotatedRect[2])

                        ellipseFit = None

                        if(size[0] >= size[1]):
                            ellipseFit = self.ellipseFuzzySet.fitLinear(size[1] / size[0])
                        elif(size[0] < size [1]):
                            ellipseFit = self.ellipseFuzzySet.fitLinear(size[0] / size[1])

                        if(ellipseFit is not None and ellipseFit >= self.ellipseFuzzySet.threshold):
                            foundBlack = 0
                            foundWhite = 0

                            for j in range (x,x+w):
                                for k in range(y,y+h):
                                    dist = cv2.pointPolygonTest(contour,(j,k),False)
                                    if dist>= 0:
                                        foundBlack += 1.0
                                        if(hsvMask[k,j] == 255):
                                            foundWhite+=1.0
                                            foundBlack-=1.0

                            colorFit = None

                            if(foundBlack > foundWhite):
                                colorFit = self.colorFuzzySet.fitLinear(foundWhite / foundBlack)
                            elif(foundWhite >= foundBlack and foundBlack > 0):
                                colorFit = 1 + self.colorFuzzySet.fitLinear(foundBlack / foundWhite)

                            if(colorFit is not None and colorFit >= self.colorFuzzySet.threshold):
                                fittingContours.append((center, size, angle,(colorFit+ellipseFit)/2, cnt))

                if len(fittingContours) > 0:
                    best = fittingContours[0]
                    for i in range (0,len(fittingContours)):
                        if(fittingContours[i][3] > best[3]):
                            best = fittingContours[i]
                    if(best[3] >= self.contourThreshold):
                        cv2.rectangle(rectImg,(int(best[0][0]-best[1][0]/2), int(best[0][1]-best[1][1]/2)),
                                     (int(best[0][0]+best[1][0]/2), int(best[0][1]+best[1][1]/2)),(255,0,0),2)

                        result.append(best)
                        fcnt += 1

                cv2.imshow('frame', rectImg)
            else:
                print "Frame not found"

            cnt += 1
            if (cnt == self.Camera.capture.get(cv2.CAP_PROP_FRAME_COUNT)) or cv2.waitKey(3) & 0xff == ord('q'):
                print 'found', fcnt, 'has', self.Camera.capture.get(cv2.CAP_PROP_FRAME_COUNT)
                return result, images

class ImageAnalyzer:

    def __init__(self):
        self.kernel = np.ones((3,3))

    def setKernel(self, morphEnum, size):
        if morphEnum == MorphologyEnumerations.ELLIPSE:
            self.kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, size)
        elif morphEnum == MorphologyEnumerations.RECTANGLE:
            self.kernel = cv2.getStructuringElement(cv2.MORPH_RECT, size)
        elif morphEnum == MorphologyEnumerations.CROSS:
            self.kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, size)

    def morphology(self, image, morphEnum):

        if morphEnum == MorphologyEnumerations.OPEN:
            return cv2.morphologyEx(image, cv2.MORPH_OPEN, self.kernel)
        elif morphEnum == MorphologyEnumerations.CLOSE:
            return cv2.morphologyEx(image, cv2.MORPH_CLOSE, self.kernel)
        elif morphEnum == MorphologyEnumerations.GRADIENT:
            return cv2.morphologyEx(image, cv2.MORPH_GRADIENT, self.kernel)
        elif morphEnum == MorphologyEnumerations.TOPHAT:
            return cv2.morphologyEx(image, cv2.MORPH_TOPHAT, self.kernel)
        elif morphEnum == MorphologyEnumerations.BLACKHAT:
            return cv2.morphologyEx(image, cv2.MORPH_BLACKHAT, self.kernel)

class MorphologyEnumerations:
    RECTANGLE,ELLIPSE,CROSS = range(3)
    OPEN,CLOSE,GRADIENT,TOPHAT,BLACKHAT = range(3,8)